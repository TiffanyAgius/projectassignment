# ProjectAssignment

Tiffany Agius MSD 4.2B

//EnemyCollision
    void OnTriggerEnter2D(Collider2D mycollision)
    {
       

        if (mycollision.gameObject.tag == "Player")
        {
            Player.playerHealth -= 5;
        }

        if(EnemyHealth <= 0)
        {
            Player.playerScore += 1;
            Destroy(this.gameObject);
        }

        if (Player.playerHealth <= 0)
        {
            lvlManager.LoadLevel("LoseScene");
        }

        if(Player.playerScore == 3)
        {
            lvlManager.LoadLevel("Level2");
        }
    }